unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls,WebFastReport,ShellAPI,ReportDataLoader,SysComm,
  ExtCtrls,EncdDecd,IdHTTP,jpeg;
type
  TForm2 = class(TForm)
    URLEdit: TEdit;
    Label1: TLabel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Label2: TLabel;
    Label3: TLabel;
    AppNameEdit: TEdit;
    ReportNameEdit: TEdit;
    Button4: TButton;
    Button5: TButton;
    Label4: TLabel;
    ParamsEdit: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function checkInput():Boolean;
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  if Self.checkInput() then
  begin
  Report:=TWebFastReport.create(Self,self.AppNameEdit.Text,Self.ReportNameEdit.Text,self.ParamsEdit.Text,self.URLEdit.Text);
  Report.designReport;
  FreeAndNil(Report);
  end;
end;

procedure TForm2.Button4Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  if Self.checkInput() then
  begin
  Report:=TWebFastReport.create(Self,self.AppNameEdit.Text,Self.ReportNameEdit.Text,self.ParamsEdit.Text,self.URLEdit.Text);
  Report.createReport;
  FreeAndNil(Report);
  end;
end;

procedure TForm2.Button2Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
   if Self.checkInput() then
  begin
  Report:=TWebFastReport.create(Self,self.AppNameEdit.Text,Self.ReportNameEdit.Text,self.ParamsEdit.Text,self.URLEdit.Text);
  Report.showReport;
  FreeAndNil(Report);
  end;
end;

procedure TForm2.Button3Click(Sender: TObject);
var
  Report:TWebFastReport;
begin
  if Self.checkInput then
  begin
  Report:=TWebFastReport.create(Self,self.AppNameEdit.Text,Self.ReportNameEdit.Text,self.ParamsEdit.Text,self.URLEdit.Text);
  Report.printReport;
  FreeAndNil(Report);
  end;
end;
function TForm2.checkInput: Boolean;
begin
  if Self.AppNameEdit.Text = '' then
  begin
    ShowMessage('请输入应用的名称！');
    Result:=False;
  end;
  if self.ReportNameEdit.Text ='' then
  begin
    ShowMessage('请输入报表的名称！');
    Result:=False;
  end;
  if Self.URLEdit.Text ='' then
  begin
    ShowMessage('请输入数据提供的地址！');
    Result:=False;
  end;
  Result:=True;
end;

procedure TForm2.Button5Click(Sender: TObject);
var
  d:AnsiString;
begin
  d:= Ansistring(TReportDataLoader.GetClientSaveRootDirectory);
   if not DirectoryExists(d) then
        ForceDirectories(d); 
  ShellExecute(Handle,'open','Explorer.exe',PAnsiChar(d),nil,1);
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  {
  self.AppNameEdit.Text:='fastreport';
  self.ReportNameEdit.Text:='billMonthDetail';
  Self.URLEdit.Text:='http://www.zjgeport.gov.cn/ZjgPortShip/statistics/statistics!printBillMonthDetail.action?berthDate=2013-02';
  }
end;
end.

