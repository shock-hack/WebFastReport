unit ReportData;

interface
uses
  SysUtils, Variants, Classes,DBClient,NativeXml,DB;
Type
  PVariable = ^RVariable;
  RVariable = record
    key:String;
    value:String;
  end;
Type
  PDataSet = ^RDataSet;
  RDataSet = record
    name:String;
    dataset:TClientDataSet;
  end;
Type
  TReportData = class
  private
    owner:TComponent;
    variables:TList;
    datasets:TList;
  public
    constructor create(AOwner: TComponent);
    destructor destroy;
    procedure load(stream:TStream);
    procedure put(key:String;value:String);overload;
    procedure put(datasetName:String;dataset:TClientDataSet);overload;
    function getVariable(key:String):String;overload;
    function getVariable(index:Integer):String;overload;
    function getRVariable(index:Integer):RVariable;
    function getDataSet(datasetName:String):TClientDataSet;overload;
    function getDataSet(index:Integer):TClientDataSet;overload;
    function getRDataSet(index:Integer):RDataSet;
    function getVariableCount():Integer;
    function getDataSetCount():Integer;
end;
implementation

{ TReportData }

procedure TReportData.put(key, value: String);
var
  pv:PVariable;
begin
  New(pv);
  pv^.key:=key;
  pv^.value:=value;
  self.variables.Add(pv);
end;
procedure TReportData.put(datasetName: String; dataset: TClientDataSet);
var
  pd:PDataSet;
begin
  New(pd);
  pd^.name := datasetName;
  pd^.dataset := dataset;
  Self.datasets.Add(pd);
end;

function TReportData.getVariable(key: String): String;
var
  i:Integer;
  r:RVariable;
begin
  for i := 0  to self.variables.Count-1 do
  begin
     if PVariable(self.variables[i])^.key = key then
     begin
       r:=PVariable(self.variables[i])^;
     end;
  end;
  result:=r.value;
end;

function TReportData.getDataSet(datasetName: String): TClientDataSet;
var
  i:Integer;
  r:RDataSet;
begin
   for i := 0 to self.datasets.Count-1 do
   begin
      if PDataSet(self.datasets[i])^.name=datasetName then
      begin
        r:=PDataSet(self.datasets[i])^;
      end;
   end;
   result:=r.dataset;
end;

constructor TReportData.create(AOwner: TComponent);
begin
  Self.owner:=AOwner;
  self.variables:=TList.Create;
  self.datasets:=TList.Create;
end;

destructor TReportData.destroy;
begin
  self.variables.Free;
  self.datasets.Free;
end;

procedure TReportData.load(stream: TStream);
var
  i,j,h:Integer;
  k,v:String;
  xml:TNativeXml;
  datastream:TFileStream;
  node,dsNode,dsfieldsNode,fieldNode,dsrowdataNode,dsrowNode:TXmlNode;
  ds:TClientDataSet;
  dsName:string;
begin
  xml:=TNativeXml.Create;
  xml.EncodingString :='Utf-8';
  xml.LoadFromStream(stream);
  //解析变量
  node:=xml.Root.FindNode('variables');
  for i :=0  to node.NodeCount-1 do
  begin
     k:=node.Nodes[i].AttributeName[0];
     v:=node.Nodes[i].AttributeValue[0];
     self.put(k,v);
  end;
  //解析数据集
  node:=xml.Root.FindNode('datasets');
  for i:=0 to node.NodeCount-1 do
  begin
    ds:=TClientDataSet.Create(self.owner);
    ds.FieldDefs.Clear;

    dsnode:=node.Nodes[i];
    dsName:=dsNode.AttributeByName['name'];
    dsfieldsNode:=dsnode.FindNode('metadata').FindNode('fields');
    //解析字段
    for  j:=0  to dsfieldsNode.NodeCount-1 do
    begin
      fieldNode:=dsfieldsNode[j];
      ds.FieldDefs.Add(fieldNode.AttributeByName['attrname'],ftstring,20);
    end;
    dsrowdataNode:=dsnode.FindNode('rowdata');
    ds.CreateDataSet;
    ds.Active;
    for  j:=0  to dsrowdataNode.NodeCount-1  do
    begin
      dsrowNode:= dsrowdataNode[j];
      ds.Append;
      for  h:=0 to ds.Fields.Count-1 do
      begin
        ds.Fields[h].AsString:= dsrowNode.AttributeByName[ds.Fields[h].FieldName];
      end;
      ds.Post;
    end;
    ds.Open;
    Self.put(dsName,ds);
  end;
  xml.Free;
end;

function TReportData.getDataSetCount: Integer;
begin
  Result:=self.datasets.Count;
end;

function TReportData.getVariableCount: Integer;
begin
  Result:=self.variables.Count;
end;

function TReportData.getDataSet(index: Integer): TClientDataSet;
begin
  Result:=PDataSet(Self.datasets.Items[index])^.dataset;
end;

function TReportData.getVariable(index: Integer): String;
begin
  Result:=PVariable(Self.variables.Items[index])^.value;
end;

function TReportData.getRDataSet(index: Integer): RDataSet;
begin
  Result:=PDataSet(Self.datasets.Items[index])^;
end;

function TReportData.getRVariable(index: Integer): RVariable;
begin
  Result:=PVariable(Self.variables.Items[index])^;
end;
end.
