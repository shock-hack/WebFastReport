program WebPrintDesigner;

uses
  Forms,
  Unit2 in 'Unit2.pas' {Form2},
  WebFastReport in '..\FastReport\WebFastReport.pas',
  SysComm in '..\FastReport\SysComm.pas',
  ReportDataLoader in '..\FastReport\ReportDataLoader.pas',
  ReportData in '..\FastReport\ReportData.pas',
  NativeXml in '..\FastReport\NativeXml.pas',
  ICS_WSocket in '..\FastReport\ICS_WSocket.pas',
  ICS_WSockBuf in '..\FastReport\ICS_WSockBuf.pas',
  ICS_HttpProt in '..\FastReport\ICS_HttpProt.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
